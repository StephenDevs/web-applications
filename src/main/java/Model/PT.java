package Model;

import java.util.ArrayList;

import static Model.DataProvider.pts;

public class PT extends Person {
    private String username;
    private String password;
    private ArrayList<Customer> customers;

    public PT() {}

    public PT(String name, int age, String gender, String username, String password) {
        super(name, age, gender);
        this.password = password;
        this.username = username;
        ArrayList<Customer> customers = new ArrayList<>();
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public PT findWithUsername(String username) {
        for (PT pt : pts) {
            if (pt.getUsername().equals(username)) {
                return pt;
            }
        }
        return null;
    }

    public void setCustomers(ArrayList<Customer> customers) {
        this.customers = customers;
    }

    public ArrayList<Customer> getCustomers() {
        return this.customers;
    }

    public void addCustomer(Customer customer) {
        this.customers.add(customer);
    }
}
