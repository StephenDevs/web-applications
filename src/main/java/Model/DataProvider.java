package Model;


import java.util.ArrayList;

public class DataProvider {
    public static ArrayList<PT> pts;
    public static ArrayList<Workout> workouts;
    public static ArrayList<Customer> customersStephen;
    public static ArrayList<Customer> customersThanh;
    public static ArrayList<Customer> allCustomers;

    static {
        PT pt = new PT("Stephen Nedd", 24, "male", "stephennedd", "password");
        PT pt1 = new PT("Thanh Do Duc", 24, "male", "thanhduc", "password");

        pts = new ArrayList<PT>();
        pts.add(pt);
        pts.add(pt1);

        Customer customer = new Customer("Javier Bush", 23, "male", 50.0, 1.75 );
        Customer customer1 = new Customer("Charlie Trujillo", 51, "female",50.0, 1.75);
        Customer customer2 = new Customer("Ean Wolfe", 37, "male",50.0, 1.75);
        Customer customer3 = new Customer("Emily Benton", 61, "female",50.0, 1.75);
        Customer customer4 = new Customer("Preston Cherry", 32, "male",50.0, 1.75);
        Customer customer5 = new Customer("Alexandra Sutton", 87, "female",50.0, 1.75);
        Customer customer6 = new Customer("Maverick Perry", 44, "male",50.0, 1.75);
        Customer customer7 = new Customer("Alia Cervantes", 62, "female",50.0, 1.75);
        Customer customer8 = new Customer("Barrack Obama", 27, "male",50.0, 1.75);
        Customer customer9 = new Customer("Kennedy Maddox", 73, "female",50.0, 1.75);
        Customer customer10 = new Customer("Hugh Hefner", 44, "male",50.0, 1.75);
        Customer customer11 = new Customer("Alia Cervantes", 62, "female",50.0, 1.75);
        Customer customer12 = new Customer("Harry Potter", 27, "male",50.0, 1.75);
        Customer customer13 = new Customer("Filette Halley", 73, "female",50.0, 1.75);

        Customer customer14 = new Customer("George Bush", 23, "male",50.0, 1.75);
        Customer customer15 = new Customer("Charlie De Melio", 51, "female",50.0, 1.75);
        Customer customer16 = new Customer("Ean Wolfe", 37, "male",50.0, 1.75);
        Customer customer17 = new Customer("Emily Blunt", 61, "female",50.0, 1.75);
        Customer customer18 = new Customer("Preston Cherry", 32, "male",50.0, 1.75);
        Customer customer19 = new Customer("Hermione Granger", 87, "female",50.0, 1.75);
        Customer customer20 = new Customer("Maverick Perry", 44, "male",50.0, 1.75);
        Customer customer21 = new Customer("Alia Cervantes", 62, "female",50.0, 1.75);
        Customer customer22 = new Customer("Donald Trump", 27, "male",50.0, 1.75);
        Customer customer23 = new Customer("Oriana Maddox", 73, "female",50.0, 1.75);
        Customer customer24 = new Customer("Maverick Logan", 44, "male",50.0, 1.75);
        Customer customer25 = new Customer("Alia Cervantes", 62, "female",50.0, 1.75);
        Customer customer26 = new Customer("Brent Tanner", 27, "male",50.0, 1.75);
        Customer customer27 = new Customer("Rosa Parks", 73, "female",50.0, 1.75);

        Workout workout = new Workout("Running", 10, 4 );
        Workout workout1 = new Workout("Situps", 25, 10);
        Workout workout2 = new Workout("Pushups", 40, 2);
        Workout workout3 = new Workout("Cycling", 23, 30);
        Workout workout4 = new Workout("Pullups", 50, 3);
        Workout workout5 = new Workout("Walking", 5, 45);

        workouts = new ArrayList<Workout>();
        workouts.add(workout);
        workouts.add(workout1);


        allCustomers = new ArrayList<Customer>();
        allCustomers.add(customer1);
        allCustomers.add(customer2);
        allCustomers.add(customer3);
        allCustomers.add(customer4);
        allCustomers.add(customer5);
        allCustomers.add(customer6);
        allCustomers.add(customer7);
        allCustomers.add(customer8);
        allCustomers.add(customer9);
        allCustomers.add(customer10);
        allCustomers.add(customer11);
        allCustomers.add(customer12);
        allCustomers.add(customer13);
        allCustomers.add(customer14);
        allCustomers.add(customer15);
        allCustomers.add(customer16);
        allCustomers.add(customer17);
        allCustomers.add(customer18);
        allCustomers.add(customer19);
        allCustomers.add(customer20);
        allCustomers.add(customer21);
        allCustomers.add(customer22);
        allCustomers.add(customer23);
        allCustomers.add(customer24);
        allCustomers.add(customer25);
        allCustomers.add(customer26);
        allCustomers.add(customer27);


        customersStephen = new ArrayList<Customer>();
        customersStephen.add(customer);
        customersStephen.add(customer1);
        customersStephen.add(customer2);
        customersStephen.add(customer3);
        customersStephen.add(customer4);
        customersStephen.add(customer5);
        customersStephen.add(customer6);
        customersStephen.add(customer7);
        customersStephen.add(customer8);
        customersStephen.add(customer9);
        customersStephen.add(customer10);
        customersStephen.add(customer11);
        customersStephen.add(customer12);
        customersStephen.add(customer13);
        pt.setCustomers(customersStephen);

        customersThanh = new ArrayList<Customer>();
        customersThanh.add(customer14);
        customersThanh.add(customer15);
        customersThanh.add(customer16);
        customersThanh.add(customer17);
        customersThanh.add(customer18);
        customersThanh.add(customer19);
        customersThanh.add(customer20);
        customersThanh.add(customer21);
        customersThanh.add(customer22);
        customersThanh.add(customer23);
        customersThanh.add(customer24);
        customersThanh.add(customer25);
        customersThanh.add(customer26);
        customersThanh.add(customer27);
        pt1.setCustomers(customersThanh);

        for (int i = 0; i < allCustomers.size() ; i++) {
            Customer c = allCustomers.get(i);
            if (i % 2 == 0) {
                c.addWorkout(workout);
                c.addWorkout(workout1);
                c.addWorkout(workout5);
            } else {
                c.addWorkout(workout2);
                c.addWorkout(workout3);
                c.addWorkout(workout4);
            }
        }
    }


    public PT getPtByName(String name) {
        for (PT pt: pts) {
            if (pt.getName().equals(name)) {
                return pt;
            }
        }
        return null;
    }

    public static ArrayList<Customer> getCustomers() {
        return allCustomers;
    }

    public ArrayList<Customer> getCustomersByPt(PT pt) {
        return pt.getCustomers();
    }

    public ArrayList<PT> getPts() {
        return pts;
    }

    public void addPt(PT pt) { pts.add(pt);}


    public ArrayList<Customer> findCustomerByName(String name) {
        ArrayList<Customer> customers = new ArrayList<>();
        for (Customer customer: allCustomers) {
            if (customer.getName().equals(name)) {
                customers.add(customer);
            }
        }
        return customers;
    }

    // for search method. (add later)

    public ArrayList<Customer> findByKeyword(String keyword, PT pt) {
        ArrayList<Customer> foundCustomers = new ArrayList<Customer>();
        for (Customer customer: getCustomersByPt(pt)) {
            if (customer.getName().contains(keyword)) {
                foundCustomers.add(customer);
            }
            return foundCustomers;
        }
        System.out.println("findByKeyword error no results");
        return foundCustomers;
    }


    public Customer getCustomerById(Integer id, ArrayList<Customer> customers) {
        for (Customer c: customers) {
            if (c.getUserId() == id) {
                return c;
            }
        }
        System.out.println("customer id error Dataproivder.getCustomerById*");
        return null;
    }

}
