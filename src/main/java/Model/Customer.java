package Model;

import static Model.DataProvider.allCustomers;
import java.util.ArrayList;
import java.util.List;


public class Customer extends Person{

    private int userId;
    private static int userIdCounter = 0;
    private ArrayList<Workout> workoutsList;
    private double weight;
    private double height;
    private PT pt;

    public Customer() {}

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public Customer(String name, int age, String gender, double weight, double height) {
        super(name, age, gender);
        this.userId = userIdCounter++;
        this.weight = weight;
        this.height = height;
        workoutsList = new ArrayList<>();
    }

    public Customer findByName(String name) {
        for (Customer customer: allCustomers) {
            if (customer.getName().equals(name)) {
                return customer;
            }
        }
        System.out.println("null customer");
        return null;
    }


    public int getUserId() {
            return userId;
        }

    public void setUserId(int userId) {
        this.userId = userId;
    }


    public void setPt(PT pt) { this.pt = pt;}

    @Override
    public String toString() {
        return "User{" +
                "userId= '" + userId + '\'' +
                '}';
    }


    public ArrayList<Workout> getWorkoutsList() {
        return workoutsList;
    }

    public void addWorkout(Workout workout) {

        this.workoutsList.add(workout);
    }


    public boolean deleteWorkout(String workoutName)
    {
        for (int i = 0; i < workoutsList.size(); i++) {
            if (workoutsList.get(i).getName().equals(workoutName))
            {
                workoutsList.remove(workoutsList.get(i));
                return true;
            }
        }
        return false;
    }


}
