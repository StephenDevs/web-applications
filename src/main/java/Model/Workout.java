package Model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Workout {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String name;
    private int caloriePerMin;
    private int duration;

    public Workout() {}

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Workout(String name, int caloriePerMin, int duration) {
        this.name = name;
        this.caloriePerMin = caloriePerMin;
        this.duration = duration;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCaloriePerMin() {
        return caloriePerMin;
    }

    public void setCaloriePerMin(int caloriePerMin) {
        this.caloriePerMin = caloriePerMin;
    }

    @Override
    public String toString() {
        return "Workout{" +
                "name='" + name + '\'' +
                ", caloriePerMin=" + caloriePerMin +
                '}';
    }
}
