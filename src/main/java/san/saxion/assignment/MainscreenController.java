package san.saxion.assignment;

import Model.DataProvider;
import Model.Customer;
import Model.PT;
import Model.Workout;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;


@Controller
@RequestMapping("/fitbuddy")
public class MainscreenController {

    DataProvider dataProvider = new DataProvider();
    PT pt;

    public MainscreenController() {
    }

    @GetMapping("")
    public String getlogin1() {
        return "login";
    }

    @GetMapping("/login")
    public String getlogin2() {
        return "login";
    }


    @RequestMapping("/createaccount")
    public String createAccount() {
        return "createaccount";
    }

    @GetMapping("/homepage")
    public String homepage( HttpSession session, Model model,HttpServletResponse res, HttpServletRequest req,
                           @CookieValue(value = "counter", defaultValue = "0") int counterValue,
                           @CookieValue(value = "counterThanh", defaultValue = "0") int counterThanh){
        if (session.isNew()) {
            return "redirect:/login";
        }
        if (session.getAttribute("name").equals("Stephen Nedd")){
            counterValue++;
            Cookie cookie = new Cookie("counter", "" + counterValue);
            res.addCookie(cookie);
            model.addAttribute("numVisits", counterValue);
        } else if (session.getAttribute("name").equals("Thanh Do Duc")) {
            counterThanh++;
            Cookie cookie = new Cookie("counterThanh", "" + counterThanh);
            res.addCookie(cookie);
            model.addAttribute("numVisits2", counterThanh);
        }


        String name = (String) session.getAttribute("name");
        pt = dataProvider.getPtByName(name);
        model.addAttribute("customers", pt.getCustomers());
        session.setAttribute("customers", pt.getCustomers());

        return "homepage";
    }

    @GetMapping("/homepage/client/{id}")
    public String homepageClient(@PathVariable("id") Integer id, Model model, HttpSession session) {
        String name = (String) session.getAttribute("name");
        pt = dataProvider.getPtByName(name);

        Customer customer = dataProvider.getCustomerById(id, dataProvider.getCustomersByPt(pt));
        ArrayList<Workout> workouts = customer.getWorkoutsList();

        model.addAttribute("customer", customer);
        model.addAttribute("customers", dataProvider.getCustomersByPt(pt) );
        model.addAttribute("workouts", workouts);
        return "homepage";
    }


    @PostMapping("/homepage")
    public String homepageSearch(String keyword, HttpSession session, Model model) {
        if (session.isNew()) {
            return "redirect:/login";
        }

        String name =(String) session.getAttribute("name");
        pt = dataProvider.getPtByName(name);
        ArrayList<Customer> test = dataProvider.findByKeyword(keyword, pt);
        if (test.size() > 0) {
            model.addAttribute("foundCustomers", dataProvider.findByKeyword(keyword, pt));
        }
        System.out.println(model.getAttribute("foundCustomers"));

        return "homepage";
    }

}
