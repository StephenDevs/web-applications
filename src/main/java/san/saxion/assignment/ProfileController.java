package san.saxion.assignment;

import Model.DataProvider;
import Model.PT;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;

@Controller
public class ProfileController {

    private ArrayList<String> validUsernames = new ArrayList<>();
    private ArrayList<String> validPassword = new ArrayList<>();
    ArrayList<PT> pts;

    public ProfileController() {
        DataProvider dataProvider = new DataProvider();
        pts = dataProvider.getPts();

        for (int i = 0; i < pts.size(); i++) {
            validUsernames.add(pts.get(i).getUsername());
            validPassword.add(pts.get(i).getPassword());
        }

    }

    @GetMapping(path = "")
    public String getProfile(HttpSession session) {
        if (!isLoggedIn(session)) {

            return "login";
        }
        return "homepage";
    }

    @GetMapping(path = "/login")
    public String getLogin(HttpSession session) {
        if (isLoggedIn(session)) { ;
            //Thymeleaf allows simple redirection by returning "redirect:" + the name of the page to redirect to
            return "redirect:/homepage";
        }
        return "login";
    }

    @GetMapping(path = "/createaccount")
    public String getCreateAccount(HttpSession session) {
        if (isLoggedIn(session)) {
            return "homepage";
        }
        return "createaccount";
    }

    @PostMapping(path = "/login")
    public String postLogin(HttpSession session, PT pt, Model model) {
        if (validUsernames.contains(pt.getUsername())) {
            PT pt1 = pt.findWithUsername(pt.getUsername());
            if (pt1.getUsername().equals(pt.getUsername()) && pt1.getPassword().equals(pt.getPassword())) {
                session.setAttribute("name", pt1.getName());


                System.out.println(pt1.getUsername());
                return "redirect:/fitbuddy/homepage";
            } else if (pt1.getUsername().equals(pt.getUsername()) && !pt1.getPassword().equals(pt.getPassword())) {
                model.addAttribute("errormessage", "password not correct!");
                return "login";
            }
        } else {
            model.addAttribute("errormessage", "username does not exist!");
            return "login";
        } return "login";
    }

    @PostMapping(path = "/logout")
    public String postLogout(HttpSession session) {
        session.setAttribute("username", "");
        return "login";
    }

    @PostMapping(path = "/createaccount")
    public String postNewAccount(HttpSession session, PT pt, Model model) {
        if (validUsernames.contains(pt.getUsername())) {
            model.addAttribute("errormessage", "Username already in use");
            System.out.println("failure1");
            return "createaccount";
        } else {
            PT ptNew = new PT(pt.getName(), 0, "noGender", pt.getUsername(), pt.getPassword());
            DataProvider.pts.add(pt);
            System.out.println("successfully created account:  "+ pt.getUsername());
            return "homepage";
        }
    }

    private boolean isLoggedIn(HttpSession session) {
        return session.getAttribute("username") != null;
    }
}
