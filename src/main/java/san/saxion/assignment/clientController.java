package san.saxion.assignment;

import Model.Customer;
import Model.DataProvider;
import Model.PT;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
public class clientController {

    DataProvider dataProvider = new DataProvider();
    PT pt;

    public clientController() {}

    @GetMapping("/add")
    public String homepageAdd(HttpSession session) {
        if (session.isNew()) {
            return "redirect:/login";
        }

        System.out.println("addclient");
        return "addclient";
    }

    @PostMapping("/addclient")
    public String addClient(Customer customer, Model model, HttpSession session) {
        if (customer != null) {
            String name = (String) session.getAttribute("name");
            pt = dataProvider.getPtByName(name);

            Customer newCustomer = new Customer(customer.getName(),customer.getAge(),customer.getGender(),customer.getWeight(),customer.getHeight());
            newCustomer.setPt(pt);

            dataProvider.getCustomersByPt(pt).add(newCustomer);
            System.out.println(newCustomer.getUserId());

            model.addAttribute("customers", dataProvider.getCustomersByPt(pt));
        }
        return  "redirect:fitbuddy/homepage";
    }
}
